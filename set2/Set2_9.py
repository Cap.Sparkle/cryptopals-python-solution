
def MakeLengthMultiplyOf(text, blockLength):
    numberToAdd = blockLength - (len(bytearray(text, "ascii")) % blockLength)
    return (text + (numberToAdd * "\x04"))
    
if __name__ == "__main__":
    
    print(MakeLengthMultiplyOf("134tgehr", 20))