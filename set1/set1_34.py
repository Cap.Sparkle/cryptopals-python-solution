 
import re
import pickle
import numpy as np
from scipy.linalg import norm
np.set_printoptions(suppress=True)
import random

def xorCypherBreaker(hexString, englishLanguageFrequencies):

	cypherTextLength = len(hexString) // 2
	
	if(len(hexString) % 2 == 1):
		print ("input length error")
		return
	
	cypherText = np.zeros((cypherTextLength), dtype = np.uint8)	
	
	i = 0 
	for byte in bytearray.fromhex(hexString):
		cypherText[i] = byte;
		i += 1
	
	numberOfUniqueCharacters = len(np.unique(cypherText))
	
	analysisTable = np.zeros((256, 256))
	
	for key in range(256):
		for letter in cypherText:
			plainChar = letter ^ key
			analysisTable[key, plainChar] += 1

	analysisTable = analysisTable / cypherTextLength * 100
	
	#====================== evaluating L2 meassure ===========================
	minimalDistanceIndexes = [100]
	minimalDistances = [100]
	
	i = 0
	for row in analysisTable:
		distance = norm(row - englishLanguageFrequencies)
			
		if distance < minimalDistances[len(minimalDistances) - 1]:
			minimalDistances.append(distance)
			minimalDistanceIndexes.append(i)
		i += 1
	
	i = 0
	answerStrings = []
	for key in minimalDistanceIndexes:
		answerStrings.append("")
		for letter in cypherText:
			answerStrings[i] += chr(int(letter) ^ int(letter))
			
		i += 1

	#bytearray(answerString, "ascii").hex()
	return answerStrings[len(answerStrings) - 1:], minimalDistances[len(minimalDistances) - 1:], minimalDistanceIndexes[len(minimalDistanceIndexes) - 1:]

	#=======================================================================


def encode12(string):
	key = (str(random.choice("0123456789abcdef")) + str(random.choice("0123456789abcdef")))
	key = bytearray.fromhex(key)
	
	cypherText = bytearray(b'')
	for symb in bytearray(string, 'ascii'):
		cypherText += bytearray([symb ^ ord(key)])
	return cypherText
	
#=============================================================================
	
	
if __name__ == "__main__":
	
	fileToAnalysis = open("analysedStrings.txt", "r")

	englishLanguageFrequencies = np.zeros((256))
	pkl_data = open('englishLanguageFrequencies.pkl', 'rb')
	englishLanguageFrequencies = pickle.load(pkl_data)
	pkl_data.close()

	
	listOfStrings = []
	
	listOfStrings.append("But i'm taking the greyhound on the Hudson River line")
	listOfStrings.append("Rain on Your Parade Meaning: To spoil someone's fun or plans; ruining a pleasurable moment")
	listOfStrings.append("On Cloud Nine Meaning: Having strong feelings of happiness or satisfaction. ")
	listOfStrings.append("Love Birds Meaning: A pair of people who have a shared love for each other.")
	listOfStrings.append("Playing Possum Meaning: Pretending to be dead, or to be deceitful about something.")
	listOfStrings.append("Down To Earth Meaning: Practical or humble; unpretentious.")
	listOfStrings.append("Beating Around the Bush Meaning: Someone who is beating around the bush is someone who avoids the main poin")
	listOfStrings.append("Back to Square One Meaning: To go back to the beginning; back to the drawing board.")
	listOfStrings.append("Right Off the Bat Meaning: Immediately, done in a hurry; without delay.")
	listOfStrings.append("On Cloud Nine Meaning: Having strong feelings of happiness or satisfaction. ")
	
	
	fileForTest = open("testText.txt", "r")
	text = fileForTest.read()
	
	sentencies = re.findall(r'[.][^.]{10,}[.]', text)
	
	[listOfStrings.append(sent[1:len(sent)-1]) for sent in sentencies] 
	
	
	for string in listOfStrings:
		answerString, minimalDistance, key = xorCypherBreaker(encode12(string).hex(), 	englishLanguageFrequencies)
		if (answerString != string):
			print('error')
		else:
			#pass
			print(str(answerString))
