 
from bitarray import bitarray

def hex2base64(hexString):
		
	base64_encodingArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	#=============================================
	
	bitVector = bitarray(endian = 'big')
	bitVector.frombytes(bytes.fromhex(hexString))
	
	bitVectorLength = len(bitVector)
	sixBitCounter = 0
	
	result = ""
	while sixBitCounter < bitVectorLength // 6:
		
		anotherOneByte = (bitarray('00') + bitVector[0 + (6 * sixBitCounter): 6 + (6 * sixBitCounter)])
		result += base64_encodingArray[ord(anotherOneByte.tobytes())]

		sixBitCounter += 1

	
	if (bitVectorLength % 6) != 0:
		lastBitArray = bitarray('00') + bitVector[sixBitCounter * 6:] + (bitarray('0') * (6 - bitVector[sixBitCounter * 6:].length()))
		result +=  base64_encodingArray[ord(lastBitArray.tobytes())]
		
	print(result)
	return result

def xor2hexes(hexString1, hexString2):
	
	if(len(hexString1) != len(hexString2)):
		print("input length error")
		return
	
	bytes1 = bytearray.fromhex(hexString1)
	bytes2 = bytearray.fromhex(hexString2)

	result = bytearray(b'')
	for byte1, byte2 in zip(bytes1, bytes2):
		result += bytearray(((byte1 ^ byte2).to_bytes(1, byteorder = 'big')))
	print(type(result))
	print(result)
	return 




xor2hexes("12312345", "deadbeef")
