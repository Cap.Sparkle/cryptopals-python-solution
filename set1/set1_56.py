
from bitarray import bitarray
from bitarray import bitdiff

import pickle


from base64 import b64decode

from set1_34 import xorCypherBreaker

import numpy as np
np.set_printoptions(suppress = True)
from itertools import combinations


def RepeatingKeyXorDecryptor(_key, _plainText):

	key = np.array(_key, dtype = np.uint8)
	plainText = np.array((_plainText), dtype = np.uint8)
	
	keyLength = len(key)
	plainTextLength = len(plainText)
	
	answer = np.zeros((plainTextLength), dtype = np.uint8)
	i = 0
	while i < plainTextLength:
		answer[i] = key[i % keyLength] ^ plainText[i]
		i += 1
	
	np.set_printoptions(formatter={'all': lambda x: hex(int(x))})
	
	stringAnswer = ""
	for symb in answer:
		stringAnswer += chr(symb)
	
	print(stringAnswer)
	
	#TODO: Make bytearray of that

#RepeatingKeyXorDecryptor("ICE", """Burning 'em, if you ain't quick and nimble
#							 I go crazy when I hear a cymbal""")



def RepeatingKeyXorBreaker(_cypherText):
	
	cypherText = bitarray(endian = 'big')
	cypherText.frombytes(_cypherText)
	
	minEditDistance = float(8)
	proposedKeyLength = 0
	distance = 0
	
	for keySize in range(2, 41):
		scale = keySize * 8
		
		distance += bitdiff(cypherText[scale * 0: scale * 1], cypherText[scale * 1: scale * 2])
		distance += bitdiff(cypherText[scale * 0: scale * 1], cypherText[scale * 2: scale * 3])
		distance += bitdiff(cypherText[scale * 0: scale * 1], cypherText[scale * 3: scale * 4])
		
		distance += bitdiff(cypherText[scale * 1: scale * 2], cypherText[scale * 2: scale * 3])
		distance += bitdiff(cypherText[scale * 1: scale * 2], cypherText[scale * 3: scale * 4])
		distance += bitdiff(cypherText[scale * 2: scale * 3], cypherText[scale * 3: scale * 4])

		distance /= (6 * keySize) 
	
		print(str(distance) + " --- " + str(keySize))
		if (distance < minEditDistance):
			minEditDistance = distance
			proposedKeyLength = keySize
	
	
	blockList = []
	
	border = len(cypherText)
	for blockNumber in range(proposedKeyLength):
		
		block = bitarray(endian = 'big')
		
		i = blockNumber * 8
		while i < border:
			
			block += cypherText[i: i + 8]
			i += (8 * proposedKeyLength)
	
		blockList.append(block)

	englishLanguageFrequencies = np.zeros((256))
	pkl_data = open('englishLanguageFrequencies.pkl', 'rb')
	englishLanguageFrequencies = pickle.load(pkl_data)
	pkl_data.close()
	
	proposedKey = bytearray(b'')
	i = 0
	
	ListAnswerStrings = []
	ListMinimalDistances = []
	ListKeys = []
	
	minimalDistances = []
	
	for block in blockList:
		answerStrings, minimalDistances, keys = xorCypherBreaker(block.tobytes().hex(),englishLanguageFrequencies)
		
		print(keys)
		ListKeys.append(keys)
		i += 1

	
	#========================== COMBINATOR ==================================
	
	KeyLengthCounters = np.zeros((proposedKeyLength), dtype = np.uint32)
	allKeyCombinations = proposedKeyLength ** 5
	
	keyIndex = 0
	
	while keyIndex < allKeyCombinations - 1:
		
		for i in range(proposedKeyLength):
			try:
				proposedKey += bytearray([int(ListKeys[i][KeyLengthCounters[i]])])
			except:
				return
		
		RepeatingKeyXorDecryptor(proposedKey, bytearray(_cypherText))
		print("\n\n=================" + str(keyIndex) + "====================\n\n")
		KeyLengthCounters[0] += 1
		keyIndex += 1
		
		for i in range(proposedKeyLength - 1):
			if len(ListKeys[i]) == KeyLengthCounters[i]:
				KeyLengthCounters[i + 1] += 1
				KeyLengthCounters[i] = 0
				
			elif ((KeyLengthCounters[i] % 5) == 0) and (KeyLengthCounters[i] != 0):
				KeyLengthCounters[i + 1] += 1
				KeyLengthCounters[i] = 0
			
			else:
				break
	
	
	
	
	
	#print(str(proposedKey) + "=============================")
	#RepeatingKeyXorDecryptor(proposedKey, bytearray(_cypherText))
	
		
		#return proposedKey





if __name__ == "__main__":
	
	f = open("set1_6_toDecode.txt", "r")
	string2Decrypt = b64decode(f.read())
	f.close()

	#print('let us go!')
	RepeatingKeyXorBreaker(string2Decrypt)
	#RepeatingKeyXorDecryptor(bytearray([32, 110, 32, 110, 32]), bytearray(string2Decrypt))









