 
from base64 import b64decode
from Crypto.Cipher import AES



def decryptAES128_ECB(cypherText, key):
	cipher = AES.new(key, AES.MODE_ECB)
	return cipher.decrypt(cypherText)



if __name__ == "__main__":

	f = open("set1_7_input.txt", "r")
	string2Decrypt = b64decode(f.read())
	f.close()
	
	print(decryptAES128_ECB(string2Decrypt, "YELLOW SUBMARINE"))
